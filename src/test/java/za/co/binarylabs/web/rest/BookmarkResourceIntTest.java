package za.co.binarylabs.web.rest;

import za.co.binarylabs.Application;
import za.co.binarylabs.domain.Bookmark;
import za.co.binarylabs.repository.BookmarkRepository;
import za.co.binarylabs.service.BookmarkService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the BookmarkResource REST controller.
 *
 * @see BookmarkResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class BookmarkResourceIntTest {

    private static final String DEFAULT_TITLE = "AAAAA";
    private static final String UPDATED_TITLE = "BBBBB";
    private static final String DEFAULT_DESCRIPTION = "AAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBB";
    private static final String DEFAULT_LOCATION = "AAAAA";
    private static final String UPDATED_LOCATION = "BBBBB";

    private static final Integer DEFAULT_PRIORITY = 1;
    private static final Integer UPDATED_PRIORITY = 2;

    private static final LocalDate DEFAULT_DATE_CREATED = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATE_CREATED = LocalDate.now(ZoneId.systemDefault());

    @Inject
    private BookmarkRepository bookmarkRepository;

    @Inject
    private BookmarkService bookmarkService;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restBookmarkMockMvc;

    private Bookmark bookmark;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        BookmarkResource bookmarkResource = new BookmarkResource();
        ReflectionTestUtils.setField(bookmarkResource, "bookmarkService", bookmarkService);
        this.restBookmarkMockMvc = MockMvcBuilders.standaloneSetup(bookmarkResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        bookmarkRepository.deleteAll();
        bookmark = new Bookmark();
        bookmark.setTitle(DEFAULT_TITLE);
        bookmark.setDescription(DEFAULT_DESCRIPTION);
        bookmark.setLocation(DEFAULT_LOCATION);
        bookmark.setPriority(DEFAULT_PRIORITY);
        bookmark.setDateCreated(DEFAULT_DATE_CREATED);
    }

    @Test
    public void createBookmark() throws Exception {
        int databaseSizeBeforeCreate = bookmarkRepository.findAll().size();

        // Create the Bookmark

        restBookmarkMockMvc.perform(post("/api/bookmarks")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(bookmark)))
                .andExpect(status().isCreated());

        // Validate the Bookmark in the database
        List<Bookmark> bookmarks = bookmarkRepository.findAll();
        assertThat(bookmarks).hasSize(databaseSizeBeforeCreate + 1);
        Bookmark testBookmark = bookmarks.get(bookmarks.size() - 1);
        assertThat(testBookmark.getTitle()).isEqualTo(DEFAULT_TITLE);
        assertThat(testBookmark.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testBookmark.getLocation()).isEqualTo(DEFAULT_LOCATION);
        assertThat(testBookmark.getPriority()).isEqualTo(DEFAULT_PRIORITY);
        assertThat(testBookmark.getDateCreated()).isEqualTo(DEFAULT_DATE_CREATED);
    }

    @Test
    public void checkTitleIsRequired() throws Exception {
        int databaseSizeBeforeTest = bookmarkRepository.findAll().size();
        // set the field null
        bookmark.setTitle(null);

        // Create the Bookmark, which fails.

        restBookmarkMockMvc.perform(post("/api/bookmarks")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(bookmark)))
                .andExpect(status().isBadRequest());

        List<Bookmark> bookmarks = bookmarkRepository.findAll();
        assertThat(bookmarks).hasSize(databaseSizeBeforeTest);
    }

    @Test
    public void checkDescriptionIsRequired() throws Exception {
        int databaseSizeBeforeTest = bookmarkRepository.findAll().size();
        // set the field null
        bookmark.setDescription(null);

        // Create the Bookmark, which fails.

        restBookmarkMockMvc.perform(post("/api/bookmarks")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(bookmark)))
                .andExpect(status().isBadRequest());

        List<Bookmark> bookmarks = bookmarkRepository.findAll();
        assertThat(bookmarks).hasSize(databaseSizeBeforeTest);
    }

    @Test
    public void getAllBookmarks() throws Exception {
        // Initialize the database
        bookmarkRepository.save(bookmark);

        // Get all the bookmarks
        restBookmarkMockMvc.perform(get("/api/bookmarks?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(bookmark.getId())))
                .andExpect(jsonPath("$.[*].title").value(hasItem(DEFAULT_TITLE.toString())))
                .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
                .andExpect(jsonPath("$.[*].location").value(hasItem(DEFAULT_LOCATION.toString())))
                .andExpect(jsonPath("$.[*].priority").value(hasItem(DEFAULT_PRIORITY)))
                .andExpect(jsonPath("$.[*].dateCreated").value(hasItem(DEFAULT_DATE_CREATED.toString())));
    }

    @Test
    public void getBookmark() throws Exception {
        // Initialize the database
        bookmarkRepository.save(bookmark);

        // Get the bookmark
        restBookmarkMockMvc.perform(get("/api/bookmarks/{id}", bookmark.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(bookmark.getId()))
            .andExpect(jsonPath("$.title").value(DEFAULT_TITLE.toString()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()))
            .andExpect(jsonPath("$.location").value(DEFAULT_LOCATION.toString()))
            .andExpect(jsonPath("$.priority").value(DEFAULT_PRIORITY))
            .andExpect(jsonPath("$.dateCreated").value(DEFAULT_DATE_CREATED.toString()));
    }

    @Test
    public void getNonExistingBookmark() throws Exception {
        // Get the bookmark
        restBookmarkMockMvc.perform(get("/api/bookmarks/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    public void updateBookmark() throws Exception {
        // Initialize the database
        bookmarkRepository.save(bookmark);

		int databaseSizeBeforeUpdate = bookmarkRepository.findAll().size();

        // Update the bookmark
        bookmark.setTitle(UPDATED_TITLE);
        bookmark.setDescription(UPDATED_DESCRIPTION);
        bookmark.setLocation(UPDATED_LOCATION);
        bookmark.setPriority(UPDATED_PRIORITY);
        bookmark.setDateCreated(UPDATED_DATE_CREATED);

        restBookmarkMockMvc.perform(put("/api/bookmarks")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(bookmark)))
                .andExpect(status().isOk());

        // Validate the Bookmark in the database
        List<Bookmark> bookmarks = bookmarkRepository.findAll();
        assertThat(bookmarks).hasSize(databaseSizeBeforeUpdate);
        Bookmark testBookmark = bookmarks.get(bookmarks.size() - 1);
        assertThat(testBookmark.getTitle()).isEqualTo(UPDATED_TITLE);
        assertThat(testBookmark.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testBookmark.getLocation()).isEqualTo(UPDATED_LOCATION);
        assertThat(testBookmark.getPriority()).isEqualTo(UPDATED_PRIORITY);
        assertThat(testBookmark.getDateCreated()).isEqualTo(UPDATED_DATE_CREATED);
    }

    @Test
    public void deleteBookmark() throws Exception {
        // Initialize the database
        bookmarkRepository.save(bookmark);

		int databaseSizeBeforeDelete = bookmarkRepository.findAll().size();

        // Get the bookmark
        restBookmarkMockMvc.perform(delete("/api/bookmarks/{id}", bookmark.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<Bookmark> bookmarks = bookmarkRepository.findAll();
        assertThat(bookmarks).hasSize(databaseSizeBeforeDelete - 1);
    }
}
