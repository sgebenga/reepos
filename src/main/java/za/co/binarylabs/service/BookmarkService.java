package za.co.binarylabs.service;

import za.co.binarylabs.domain.Bookmark;
import za.co.binarylabs.repository.BookmarkRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing Bookmark.
 */
@Service
public class BookmarkService {

    private final Logger log = LoggerFactory.getLogger(BookmarkService.class);
    
    @Inject
    private BookmarkRepository bookmarkRepository;
    
    /**
     * Save a bookmark.
     * @return the persisted entity
     */
    public Bookmark save(Bookmark bookmark) {
        log.debug("Request to save Bookmark : {}", bookmark);
        Bookmark result = bookmarkRepository.save(bookmark);
        return result;
    }

    /**
     *  get all the bookmarks.
     *  @return the list of entities
     */
    public Page<Bookmark> findAll(Pageable pageable) {
        log.debug("Request to get all Bookmarks");
        Page<Bookmark> result = bookmarkRepository.findAll(pageable); 
        return result;
    }

    /**
     *  get one bookmark by id.
     *  @return the entity
     */
    public Bookmark findOne(String id) {
        log.debug("Request to get Bookmark : {}", id);
        Bookmark bookmark = bookmarkRepository.findOne(id);
        return bookmark;
    }

    /**
     *  delete the  bookmark by id.
     */
    public void delete(String id) {
        log.debug("Request to delete Bookmark : {}", id);
        bookmarkRepository.delete(id);
    }
}
