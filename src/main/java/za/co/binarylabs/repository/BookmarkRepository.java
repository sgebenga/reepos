package za.co.binarylabs.repository;

import za.co.binarylabs.domain.Bookmark;

import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Spring Data MongoDB repository for the Bookmark entity.
 */
public interface BookmarkRepository extends MongoRepository<Bookmark,String> {

}
