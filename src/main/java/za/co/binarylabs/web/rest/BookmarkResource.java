package za.co.binarylabs.web.rest;

import com.codahale.metrics.annotation.Timed;
import za.co.binarylabs.domain.Bookmark;
import za.co.binarylabs.service.BookmarkService;
import za.co.binarylabs.web.rest.util.HeaderUtil;
import za.co.binarylabs.web.rest.util.PaginationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Bookmark.
 */
@RestController
@RequestMapping("/api")
public class BookmarkResource {

    private final Logger log = LoggerFactory.getLogger(BookmarkResource.class);
        
    @Inject
    private BookmarkService bookmarkService;
    
    /**
     * POST  /bookmarks -> Create a new bookmark.
     */
    @RequestMapping(value = "/bookmarks",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Bookmark> createBookmark(@Valid @RequestBody Bookmark bookmark) throws URISyntaxException {
        log.debug("REST request to save Bookmark : {}", bookmark);
        if (bookmark.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("bookmark", "idexists", "A new bookmark cannot already have an ID")).body(null);
        }
        Bookmark result = bookmarkService.save(bookmark);
        return ResponseEntity.created(new URI("/api/bookmarks/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("bookmark", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /bookmarks -> Updates an existing bookmark.
     */
    @RequestMapping(value = "/bookmarks",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Bookmark> updateBookmark(@Valid @RequestBody Bookmark bookmark) throws URISyntaxException {
        log.debug("REST request to update Bookmark : {}", bookmark);
        if (bookmark.getId() == null) {
            return createBookmark(bookmark);
        }
        Bookmark result = bookmarkService.save(bookmark);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("bookmark", bookmark.getId().toString()))
            .body(result);
    }

    /**
     * GET  /bookmarks -> get all the bookmarks.
     */
    @RequestMapping(value = "/bookmarks",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<Bookmark>> getAllBookmarks(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of Bookmarks");
        Page<Bookmark> page = bookmarkService.findAll(pageable); 
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/bookmarks");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /bookmarks/:id -> get the "id" bookmark.
     */
    @RequestMapping(value = "/bookmarks/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Bookmark> getBookmark(@PathVariable String id) {
        log.debug("REST request to get Bookmark : {}", id);
        Bookmark bookmark = bookmarkService.findOne(id);
        return Optional.ofNullable(bookmark)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /bookmarks/:id -> delete the "id" bookmark.
     */
    @RequestMapping(value = "/bookmarks/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteBookmark(@PathVariable String id) {
        log.debug("REST request to delete Bookmark : {}", id);
        bookmarkService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("bookmark", id.toString())).build();
    }
}
