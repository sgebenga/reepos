'use strict';

angular.module('reeposApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('bookmark', {
                parent: 'entity',
                url: '/bookmarks',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'reeposApp.bookmark.home.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/bookmark/bookmarks.html',
                        controller: 'BookmarkController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('bookmark');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
            .state('bookmark.detail', {
                parent: 'entity',
                url: '/bookmark/{id}',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'reeposApp.bookmark.detail.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/bookmark/bookmark-detail.html',
                        controller: 'BookmarkDetailController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('bookmark');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'Bookmark', function($stateParams, Bookmark) {
                        return Bookmark.get({id : $stateParams.id});
                    }]
                }
            })
            .state('bookmark.new', {
                parent: 'bookmark',
                url: '/new',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'scripts/app/entities/bookmark/bookmark-dialog.html',
                        controller: 'BookmarkDialogController',
                        size: 'lg',
                        resolve: {
                            entity: function () {
                                return {
                                    title: null,
                                    description: null,
                                    location: null,
                                    priority: null,
                                    dateCreated: null,
                                    id: null
                                };
                            }
                        }
                    }).result.then(function(result) {
                        $state.go('bookmark', null, { reload: true });
                    }, function() {
                        $state.go('bookmark');
                    })
                }]
            })
            .state('bookmark.edit', {
                parent: 'bookmark',
                url: '/{id}/edit',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'scripts/app/entities/bookmark/bookmark-dialog.html',
                        controller: 'BookmarkDialogController',
                        size: 'lg',
                        resolve: {
                            entity: ['Bookmark', function(Bookmark) {
                                return Bookmark.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('bookmark', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            })
            .state('bookmark.delete', {
                parent: 'bookmark',
                url: '/{id}/delete',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'scripts/app/entities/bookmark/bookmark-delete-dialog.html',
                        controller: 'BookmarkDeleteController',
                        size: 'md',
                        resolve: {
                            entity: ['Bookmark', function(Bookmark) {
                                return Bookmark.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('bookmark', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            });
    });
