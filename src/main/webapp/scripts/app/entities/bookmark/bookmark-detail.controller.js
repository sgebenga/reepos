'use strict';

angular.module('reeposApp')
    .controller('BookmarkDetailController', function ($scope, $rootScope, $stateParams, entity, Bookmark) {
        $scope.bookmark = entity;
        $scope.load = function (id) {
            Bookmark.get({id: id}, function(result) {
                $scope.bookmark = result;
            });
        };
        var unsubscribe = $rootScope.$on('reeposApp:bookmarkUpdate', function(event, result) {
            $scope.bookmark = result;
        });
        $scope.$on('$destroy', unsubscribe);

    });
