'use strict';

angular.module('reeposApp').controller('BookmarkDialogController',
    ['$scope', '$stateParams', '$uibModalInstance', 'entity', 'Bookmark',
        function($scope, $stateParams, $uibModalInstance, entity, Bookmark) {

        $scope.bookmark = entity;
        $scope.load = function(id) {
            Bookmark.get({id : id}, function(result) {
                $scope.bookmark = result;
            });
        };

        var onSaveSuccess = function (result) {
            $scope.$emit('reeposApp:bookmarkUpdate', result);
            $uibModalInstance.close(result);
            $scope.isSaving = false;
        };

        var onSaveError = function (result) {
            $scope.isSaving = false;
        };

        $scope.save = function () {
            $scope.isSaving = true;
            if ($scope.bookmark.id != null) {
                Bookmark.update($scope.bookmark, onSaveSuccess, onSaveError);
            } else {
                Bookmark.save($scope.bookmark, onSaveSuccess, onSaveError);
            }
        };

        $scope.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
        $scope.datePickerForDateCreated = {};

        $scope.datePickerForDateCreated.status = {
            opened: false
        };

        $scope.datePickerForDateCreatedOpen = function($event) {
            $scope.datePickerForDateCreated.status.opened = true;
        };
}]);
