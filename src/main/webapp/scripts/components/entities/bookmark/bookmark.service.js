'use strict';

angular.module('reeposApp')
    .factory('Bookmark', function ($resource, DateUtils) {
        return $resource('api/bookmarks/:id', {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    data.dateCreated = DateUtils.convertLocaleDateFromServer(data.dateCreated);
                    return data;
                }
            },
            'update': {
                method: 'PUT',
                transformRequest: function (data) {
                    data.dateCreated = DateUtils.convertLocaleDateToServer(data.dateCreated);
                    return angular.toJson(data);
                }
            },
            'save': {
                method: 'POST',
                transformRequest: function (data) {
                    data.dateCreated = DateUtils.convertLocaleDateToServer(data.dateCreated);
                    return angular.toJson(data);
                }
            }
        });
    });
